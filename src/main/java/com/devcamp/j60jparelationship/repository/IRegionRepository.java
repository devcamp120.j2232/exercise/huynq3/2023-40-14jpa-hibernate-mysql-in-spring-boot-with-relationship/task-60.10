package com.devcamp.j60jparelationship.repository;

import com.devcamp.j60jparelationship.model.CRegion;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {

}
