package com.devcamp.j60jparelationship.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import com.devcamp.j60jparelationship.model.CRegion;
import com.devcamp.j60jparelationship.repository.IRegionRepository;

import org.springframework.stereotype.Service;

@Service
public class RegionService {
    @Autowired
	IRegionRepository pRegionRepository;

    public ArrayList<CRegion> getAllRegions() {
        ArrayList<CRegion> regions = new ArrayList<>();
        
        List<CRegion> pRegions = new ArrayList<CRegion>();

		pRegionRepository.findAll().forEach(pRegions::add);

        return regions;
    }
}
